

## Jumpers

### JP1/JP2

1        - SDA
2  3     - GND  SCL
4  5     - +5V  +3.3V


### JP3

1 - GND
2 - GPIO-9
3 - GPIO-10
4 - BOOT1

### JP4

1 - Unregulated power
2 - GND

### JP5

#### V0.3 (was called JP6)
1 - RESET
2 - AIN
3 - GPIO-2
4 - GPIO-3
5 - GPIO-4
6 - GND

#### V0.5
1 - Analog In
2 - GND
3 - GPIO-2
4 - GPIO-3
5 - GPIO-4

### JP6

#### V0.3 (was called JP5)
1 - UART/TX
2 - UART/RX
3 - GND
4 - BOOT0
5 - BOOT1

#### V0.5
1 - RESET
2 - UART/TX
3 - UART/RX
4 - GND
5 - BOOT0
6 - BOOT1

