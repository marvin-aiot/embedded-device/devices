

## License

All the hardware and software in this project is released under the GPL Version 3 or later.

>    Copyright (C) 2019 Eric Diethelm
>
>    This program is free software: you can redistribute it and/or modify
>    it under the terms of the GNU General Public License as published by
>    the Free Software Foundation, either version 3 of the License, or
>    (at your option) any later version.
>
>    This program is distributed in the hope that it will be useful,
>    but WITHOUT ANY WARRANTY; without even the implied warranty of
>    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
>    GNU General Public License for more details.
>
>    You should have received a copy of the GNU General Public License
>    along with this program.  If not, see https://www.gnu.org/licenses/.


## Structure

Each folder contains the code, schematics and enclosure design for a Generic Device; usually for more than one hardware platform. 


## List of Generic Devices

### air_quality_sensor

Measures the the concentration of dust (through an optical dust sensor such as the GP2Y1010AU0F), a variety of noxious gases through sensors such as the MQ2 or MQ135 and the air temperature and humidity via a DTH22.


### bed_movement_sensor

Based on a MPU6050 this device is capable of detecting movement on the bed (e.g. for sleep quality monitoring) and indicates whether that bed is occupied. This device can also be used to monitor other furniture (e.g. couches or chairs).


### dimmer_1ch



### dimmer_RGBW

LED dimmer with four (4) channels capable of 12V or 24V.

### doorbell

A doorbell that also acts as an intercom with video.


### env_internal_sensor

A compact device to measure air temperature and humidity (DTH22), movement (PIR sensor) and luminosity (LDR). Offers two (2) digital inputs to connect door or window contacts.


### env_external_sensor

A compact device to measure air temperature and humidity (DTH22), luminosity (LDR) and air pressure (BMP280). Wind speed, wind direction, rain intensity and UV index can be added.


### heater_actor_monitor

This device is actually a gateway to heater thermostats eQ-3 over Bluetooth.


### ir_transciever

This device acts as a gateway for sending and receiving infra-red commands. It maps Marvin events to IR sequences.


### power_supply

Specially in CAN wired installations (but also valid for wire-less ones) the other devices are powered by this device. It also acts as a UPS for the network when connected to an external battery.


### relais_1ch



### relais_4ch



### sonoff_basic



### sonoff_switch



### time_service_DFC77

Using a DFC77 receiver, this module is capable of generating events to synchronize the clock on all Devices in a network. It is expected that time in the network is correct to 0.1 seconds.
Note: This is not the only Device capable of synchronization (e.g. a Marvin Device with NTP), but in a network only one is sending synchronization events (although a backup Device can exist).


### touch_switch

A touch wall-switch with five (5) sectors (top-left, bottom-left, bottom-right, top-right and center). Each sector can be associated with an event (payload) and also gestures can be defined (e.g. left top-to-bottom, top right-to-left).


