/*
 * Settings
 *
 * Generic ESP8265
 * 80MHz
 * 1M (64K SPIFFS)
 * Flash Mode DIO
 *
 */
#include <marvin.h>

namespace marvin
{

const char* json_contact        PROGMEM = "CONTACT";
const char* json_luminosity     PROGMEM = "LUMINOSITY";
const char* json_movement       PROGMEM = "MOVEMENT";
const char* json_state          PROGMEM = "STATE";

enum operating_mode_t
{
  master,
  slave
};

enum params
{
  state_send_interval,
  contact_send_interval,
  contact,
  ilumination_source,
  movement_source,
  long_press_interval,
  luminosity,
  luminosity_threshold,
  movement,
  relais_timeout,
  operating_mode,
  auto_mode_disabled,
  relais_state,
};

enum relais_state_t
{
  off,
  manual_on,
  auto_on
};

static const uint8_t  contact_pin               = 0;     // GPIO not avaliable on standard Marvin Devices
static const uint8_t  relais_pin                = GPIO3; // Pin 12
static const uint8_t  switch_led_pin            = GPIO6; // Pin 4
static const uint8_t  wifi_led_pin              = GPIO4; // Pin 13
static const uint16_t switch_led_blink_time     = 500;

static int            switch_led_state       = LOW;
static bool           lpd_initiated = false;

static const uint32_t inactivity_timeout     = 3000;

define_message_generator(547,
{
  Message msg = make_message(547);
  msg->set_payload(json_state, get<relais_state_t>(relais_state));
  success = send(msg);
});

define_message_generator(580,
{
  Message msg = make_message(580);
  msg->set_payload(json_contact, get<bool>(contact, true));
  success = send(msg);
});

void handle_lux_change()
{
  bool lux_below_threshold = get<uint16_t>(luminosity, true) <
                             get<uint16_t>(luminosity_threshold, true);

  // Check whether we schould turn the relais on.
  if (lux_below_threshold &&
      get<relais_state_t>(relais_state) == off &&
      get<bool>(auto_mode_disabled) == false &&
      get<bool>(movement) == true)
  {
    set<relais_state_t>(relais_state, auto_on);
  }
  else
  {
    switch_led_state = (get<relais_state_t>(relais_state) == off &&
                        lux_below_threshold)
                       ? HIGH : LOW;
    digitalWrite(switch_led_pin, switch_led_state);
  }
}

create_delayed_timer(inactivity_time,
  {
    set<bool>(auto_mode_disabled, false);
    disable_task(timer_id_from_timer_name(inactivity_time));
  });

create_delayed_timer(led_blink_time,
  {
    switch_led_state = (switch_led_state == LOW ? HIGH : LOW);
    digitalWrite(switch_led_pin, switch_led_state);
  });

create_delayed_timer(longpress_detect,
  {
    set<relais_state_t>(relais_state, manual_on);
    disable_task(timer_id_from_timer_name(longpress_detect));
  });

create_delayed_timer(auto_on_timeout,
  {
    set<relais_state_t>(relais_state, off);
    disable_task(timer_id_from_timer_name(auto_on_timeout));
  });
  
create_onchange_task(movement,
  {
    if (get<bool>(auto_mode_disabled) == true ||
        get<bool>(movement, true) == false)
    {
      return;
    }

    relais_state_t state = get<relais_state_t>(relais_state);

    if (state == auto_on)
    {
      reschedule_task_by_id(timer_id_from_timer_name(auto_on_timeout), NOW_MILLIS + get<uint32_t>(relais_timeout));
    }
    else if (state == off &&
             (get<uint16_t>(luminosity) <
              get<uint16_t>(luminosity_threshold)))
    {
      set<relais_state_t>(relais_state, auto_on);
    }});
    
create_onchange_task(contact,
  {
    if (get<bool>(contact, true) == true)
    {
      if (get<relais_state_t>(relais_state) != manual_on)
      {
        reschedule_task_by_id(timer_id_from_timer_name(longpress_detect), NOW_MILLIS + get<uint16_t>(long_press_interval));
        enable_task(timer_id_from_timer_name(longpress_detect));
        lpd_initiated = true;
      }
    }
    else
    {
      disable_task(timer_id_from_timer_name(longpress_detect));

      if (get<relais_state_t>(relais_state) == off)
      {
        set<relais_state_t>(relais_state, auto_on);
      }
      else if (get<relais_state_t>(relais_state) == auto_on ||
               (!lpd_initiated && get<relais_state_t>(relais_state) == manual_on))
      {
        set<relais_state_t>(relais_state, off);
      }

      lpd_initiated = false;
    }
  });

create_onchange_task(luminosity,
  {
      handle_lux_change();
  });

create_onchange_task(luminosity_threshold,
  {
      handle_lux_change();
  });

create_onchange_task(relais_state,
  {
    relais_state_t state = get<relais_state_t>(relais_state, true);

    disable_task(timer_id_from_timer_name(led_blink_time));
    disable_task(timer_id_from_timer_name(auto_on_timeout));

    digitalWrite(relais_pin, (state == off) ? LOW : HIGH);

    if (state == off)
    {
      // @REVIEW Is this needed or would luminosity "on change" handle this?
      switch_led_state = (get<uint16_t>(luminosity, true) <
                          get<uint16_t>(luminosity_threshold, true))
                         ? HIGH : LOW;
    }
    else if (state == auto_on)
    {
      switch_led_state = LOW;
      reschedule_task_by_id(timer_id_from_timer_name(auto_on_timeout), NOW_MILLIS + get<uint32_t>(relais_timeout));
      enable_task(timer_id_from_timer_name(auto_on_timeout));
    }
    else if (state == manual_on)
    {
      switch_led_state =  HIGH;
      reschedule_task_by_id(timer_id_from_timer_name(led_blink_time), NOW_MILLIS + switch_led_blink_time);
      enable_task(timer_id_from_timer_name(led_blink_time));
    }

    digitalWrite(switch_led_pin, switch_led_state);
  });
  
define_interrupt_handler(contact_interrupt,
{
  set_iff<bool>(contact, (digitalRead(contact_pin) == LOW));
});

uint16_t parse_ilumination_message(const Message msg)
{
  return msg->get_payload<uint16_t>(json_luminosity);
}

bool parse_movement_message(const Message msg)
{
  return msg->get_payload<bool>(json_movement);
}

void on_run_level_changed(run_level_t run_level)
{
    if (run_level == RL0)
    {
      detachInterrupt(digitalPinToInterrupt(contact_pin));

      set<relais_state_t>(relais_state, off);
      
      disable_onchange_task(contact);
      disable_onchange_task(relais_state);
      disable_delayed_timer(led_blink_time);
      disable_delayed_timer(longpress_detect);
      disable_delayed_timer(auto_on_timeout);
    }

    if (run_level >= RL1)
    {
      // Enable all interrupts for RL1 and RL2
      attachInterrupt(digitalPinToInterrupt(contact_pin), contact_interrupt, CHANGE);

      enable_onchange_task(contact);
      enable_onchange_task(relais_state);
    }
    
    if (run_level <= RL1)
    {
      digitalWrite(relais_pin, LOW);
      digitalWrite(switch_led_pin, LOW);
      digitalWrite(wifi_led_pin, LOW);
      
      // Disable all timers
      disable_message_generator(547);
      disable_message_generator(580);

      // This timers react to incomming messages, thus being disabled
      disable_onchange_task(movement);
      disable_onchange_task(luminosity);
      disable_onchange_task(luminosity_threshold);
      disable_delayed_timer(inactivity_time);
    }
    else
    {
      digitalWrite(relais_pin, LOW);
      digitalWrite(switch_led_pin, LOW);
      digitalWrite(wifi_led_pin, HIGH);
      
      // Enable all timers for RL2
      enable_onchange_task(movement);
      enable_onchange_task(luminosity);
      enable_onchange_task(luminosity_threshold);

      enable_message_generator(547);
      enable_message_generator(580);

      send(547);
      send(580);

      handle_lux_change();
    }
}

bool initialize()
{
  // Device initialization
  pinMode(contact_pin, INPUT);
  pinMode(relais_pin, OUTPUT);
  pinMode(switch_led_pin, OUTPUT);
  pinMode(wifi_led_pin, OUTPUT);

  digitalWrite(relais_pin, LOW);
  digitalWrite(switch_led_pin, LOW);
  digitalWrite(wifi_led_pin, HIGH);

  register_parameter<uint32_t>            ("contact_send_interval", contact_send_interval,    "", 300000);
  register_parameter<MessageSource>       ("ilumination_source",    ilumination_source,       "", MessageSource(0, 0));
  register_parameter<MessageSource>       ("movement_source",       movement_source,          "", MessageSource(0, 0));
  register_parameter<uint16_t>            ("long_press_interval",   long_press_interval,      "", 750);
  register_parameter<uint16_t>            ("luminosity_threshold",  luminosity_threshold,     "", 420);
  register_parameter<uint32_t>            ("state_send_interval",   state_send_interval,      "", 300000);
  register_parameter<uint32_t>            ("relais_timeout",        relais_timeout,           "", 60000);
  register_parameter<operating_mode_t>    ("operating_mode",        operating_mode,           "", master);

  register_variable<bool>                 ("contact",               contact,                  "", false);
  register_variable<bool>                 ("movement",              movement,                 "", false);
  register_variable<uint16_t>             ("luminosity",            luminosity,               "", 0);
  register_variable<bool>                 ("auto_mode_disabled",    auto_mode_disabled,       "", false);
  register_variable<relais_state_t>       ("relais_state",          relais_state,             "", off);

  register_message_source<uint16_t>       (ilumination_source,      luminosity,   (uint16_t (*)(const Message)) parse_ilumination_message);
  register_message_source<bool>           (movement_source,         movement,     (bool (*)(const Message)) parse_movement_message);

  register_message_generator(547, get<uint32_t>(state_send_interval));
  register_message_generator(580, get<uint32_t>(contact_send_interval));

  send_on_value_changed(580, contact);
  send_on_value_changed(547, relais_state);

  initialize_delayed_timer(inactivity_time, inactivity_timeout);
  initialize_delayed_timer(led_blink_time, switch_led_blink_time);
  initialize_delayed_timer(longpress_detect, get<uint16_t>(long_press_interval));
  initialize_delayed_timer(auto_on_timeout, get<uint32>(relais_timeout));

  initialize_onchange_task(movement);
  initialize_onchange_task(contact);
  initialize_onchange_task(luminosity);
  initialize_onchange_task(luminosity_threshold);
  initialize_onchange_task(relais_state);

  return true;
}

IRAM_ATTR
void step()
{
}

}
