
#include <eQ-3.h>
#include <HM-10.h>
#include <marvin_device.h>


namespace marvin
{

extern Marvin* marvin;

enum params
{
  list_of_heaters,      // a list of BLE names, pairing keys and IDs
  update_send_interval, // frequency to send infos of heaters


};

static HM10::HM10 hm10 = HM10::HM10();

//static const unsigned int wifi_led_pin   = 13;

//static const unsigned long  switch_led_blink_time     = 500;

//static unsigned long  last_action            = 0;

void send_switch_msg()
{
  //DeviceMessage580 message = DeviceMessage580(marvin->get_device_id(), marvin->get<bool>(contact));

  //marvin->send(&message);
  //last_contact_sent = marvin->now();
}

void step()
{
  //unsigned long now = marvin->now();
}

bool initialize()
{
  // Device initialization
  hm10.setup();

  //marvin->register_parameter<unsigned long>       ("state_send_interval",   state_send_interval,      "", def_state_send_interval);

  return true;
}

bool shutdown()
{
  //digitalWrite(relais_pin, LOW);
  return true;
}

}
