
# Internal Environment Sensor

## Application

A compact device to measure air temperature and humidity (DTH22), movement (PIR sensor) and luminosity (LDR). Offers two (2) digital inputs to connect door or window contacts.

## Inputs & Outputs

### Sensor inputs

- Temperature
- Humidity
- Luminosity

### Digital Inputs

- Digital Input 1
- Digital Input 2

## Device Configuration

### System Parameters

| Name | ID | Data type | Default value | Description |
|------|----|-----------|---------------|-------------|
| hb_send_interval | 0101 | uint32 | 30000 | |
| ap_ssid | 1000 | string | | |
| ap_passwd | 1001 | string | | |
| device_id | 9000 | uint8 | 0 | |
| device_status | 9001 | | 0 | |

### System Variables

| Name | ID | Data type | Default value | Description |
|------|----|-----------|---------------|-------------|
| restart_counter | 0100 | uint8 | 0 | |

### Application-specific Parameters

| Name | ID | Data type | Default value | Description |
|------|----|-----------|---------------|-------------|
| temp_send_interval | 0000 | uint32 | 300000 | |
| temp_hyst | 0001 | uint16 | 100 | |
| humi_send_interval | 0002 | uint32 | 300000 | |
| humi_hyst | 0003 | uint16 | 100 | |
| lux_send_interval | 0004 | uint32 | 300000 | |
| lux_hyst | 0005 | uint16 | 100 | |
| movement_send_interval | 0006 | uint16 | 60000 | |
| digital_input_send_interval | 0008 | uint16 | 60000 | |

### Application-specific Variables

| Name | ID | Data type | Default value | Description |
|------|----|-----------|---------------|-------------|
| movement | 0007 | bool | false | |
| di1 | 0009  | bool | false | |
| di2 | 0010  | bool | false | |
