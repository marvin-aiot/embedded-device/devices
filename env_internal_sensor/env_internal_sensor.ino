/*
 * Settings
 *
 * Generic ESP8266
 * 80MHz
 * 1M (64K SPIFFS) or 4M (1M SPIFFS)
 * Flash Mode DIO
 * nonos-sdk 2.2.1+119
 *
 *
 *
 * @REVIEW Digital inputs have inverted logic!
 *         di1_interrupt is not responding!
 */

#include <DHT.h>
#include <marvin.h>

namespace marvin
{

const char* json_humidity       PROGMEM = "HUMIDITY";
const char* json_luminosity     PROGMEM = "LUMINOSITY";
const char* json_movement       PROGMEM = "MOVEMENT";
const char* json_state          PROGMEM = "STATE";
const char* json_temperature    PROGMEM = "TEMPERATURE";

// When compiling for older generations of hardware set 'prototype_board' to true
#define prototype_board false

#if prototype_board
/* Needed only for the sensor board prototypes (not PCB) */
static const uint8_t jp10_pin = GPIO6;
static const uint8_t movement_trigger_pin = GPIO7;
static const uint8_t dht_pin = GPIO2;
#else
/* For all other generations of the sensor board */
static const uint8_t movement_trigger_pin = GPIO4;
static const uint8_t dht_pin = GPIO7;
static const uint8_t jp10_pin = GPIO2;
#endif

static const uint8_t jp11_pin = GPIO3;

enum params
{
  temp_send_interval,
  temp_hyst,
  humi_send_interval,
  humi_hyst,
  lux_send_interval,
  lux_hyst,
  movement_send_interval,
  movement,
  digital_input_send_interval,
  di1,
  di2
};

static DHT dht(dht_pin, DHT22);

static uint16_t   temperature        = 0;
static uint16_t   last_temp_value    = 0;
static uint16_t   humidity           = 0;
static uint16_t   last_humi_value    = 0;
static uint16_t   luminosity         = 0;
static uint16_t   last_lux_value     = 0;

static task_id_t analog_read_task = 0;
static task_id_t dht_update_task = 0;

IRAM_ATTR
void update_dht_sensor(task_id_t, void*)
{
  float h = dht.readHumidity();
  float t = dht.readTemperature();

  humidity = isnan(h) ? 0 : floor(655 * h);
  temperature = isnan(t) ? 0 : floor((100 * t) + 27300);

  if (isnan(h) || isnan(t))
  {
    LOG_ERROR(F("Error reading DHT data."));
  }
}

define_message_generator(560,
{
  Message msg = make_message(560);
  msg->set_payload(json_temperature, temperature);

  success = send(msg);
  last_temp_value = temperature;
});

define_message_generator(561,
{
  Message msg = make_message(561);
  msg->set_payload(json_humidity, humidity);

  success = send(msg);
  last_humi_value = humidity;
});

define_message_generator(562,
{
  Message msg = make_message(562);
  msg->set_payload(json_luminosity, luminosity);

  success = send(msg);
  last_lux_value = luminosity;
});

define_message_generator(230,
{
  Message msg = make_message(230);
  msg->set_payload(json_movement, get<bool>(movement, true));

  success = send(msg);
});

define_message_generator(240,
{
  Message msg = make_message(240);
  msg->set_payload(json_state, get<bool>(di1, true));

  success = send(msg);
});

define_message_generator(241,
{
  Message msg = make_message(241);
  msg->set_payload(json_state, get<bool>(di2, true));

  success = send(msg);
});

define_interrupt_handler(movement_interrupt,
{
  set<bool>(movement, digitalRead(movement_trigger_pin) != 0);
});

define_interrupt_handler(di1_interrupt,
{
  set<bool>(di1, digitalRead(jp10_pin) != 0);
});

define_interrupt_handler(di2_interrupt,
{
  set<bool>(di2, digitalRead(jp11_pin) != 0);
});

void on_run_level_changed(run_level_t run_level)
{
    if (run_level == RL2)
    {
      // Enable all interrupts and timers
      attachInterrupt(digitalPinToInterrupt(movement_trigger_pin), movement_interrupt, CHANGE);
      attachInterrupt(digitalPinToInterrupt(jp10_pin), di1_interrupt, CHANGE);
      attachInterrupt(digitalPinToInterrupt(jp11_pin), di2_interrupt, CHANGE);

      update_dht_sensor(0, NULL);
      luminosity = (uint16_t) analogRead(A0);

      enable_task(analog_read_task);
      enable_task(dht_update_task);

      enable_message_generator(230);
      enable_message_generator(240);
      enable_message_generator(241);
      enable_message_generator(560);
      enable_message_generator(561);
      enable_message_generator(562);

      // Send out all sensor messages
      send(230);
      send(240);
      send(241);
      send(560);
      send(561);
      send(562);
    }
    else
    {
      // Disable all interrupts and timers in RL0 and RL1
      
      disable_task(analog_read_task);
      disable_task(dht_update_task);
      
      disable_message_generator(230);
      disable_message_generator(240);
      disable_message_generator(241);
      disable_message_generator(560);
      disable_message_generator(561);
      disable_message_generator(562);
      
      detachInterrupt(digitalPinToInterrupt(movement_trigger_pin));
      detachInterrupt(digitalPinToInterrupt(jp10_pin));
      detachInterrupt(digitalPinToInterrupt(jp11_pin)); 
    }
}

bool initialize()
{
  // Device initialization
  dht.begin();
  pinMode(movement_trigger_pin, INPUT);
  pinMode(jp10_pin, INPUT);
  pinMode(jp11_pin, INPUT);

  register_parameter<uint32_t>  ("temp_send_interval",     temp_send_interval,          "",       300000);
  register_parameter<uint16_t>  ("temp_hyst",              temp_hyst,                   "",       100);
  register_parameter<uint32_t>  ("humi_send_interval",     humi_send_interval,          "",       300000);
  register_parameter<uint16_t>  ("humi_hyst",              humi_hyst,                   "",       100);
  register_parameter<uint32_t>  ("lux_send_interval",      lux_send_interval,           "",       300000);
  register_parameter<uint16_t>  ("lux_hyst",               lux_hyst,                    "",       100);
  register_parameter<uint32_t>  ("movement_send_interval", movement_send_interval,      "",       60000);
  register_parameter<uint32_t>  ("din_send_interval",      digital_input_send_interval, "",       60000);
  register_variable<bool>       ("movement",               movement,                    "",       false);
  register_variable<bool>       ("di1",                    di1,                         "",       false);
  register_variable<bool>       ("di2",                    di2,                         "",       false);

  register_message_generator(230, get<uint32_t>(movement_send_interval));
  register_message_generator(240, get<uint32_t>(digital_input_send_interval));
  register_message_generator(241, get<uint32_t>(digital_input_send_interval));
  register_message_generator(560, get<uint32_t>(temp_send_interval));
  register_message_generator(561, get<uint32_t>(humi_send_interval));
  register_message_generator(562, get<uint32_t>(lux_send_interval));

  // @TODO This timers must be controlled within on_run_level_changed
  dht_update_task = define_delayed_task(&update_dht_sensor, 5000, true, NULL, false);

  analog_read_task = define_delayed_task([](task_id_t, void*) -> void
  {
    luminosity = (uint16_t) analogRead(A0);
  }, 1000, true, NULL, false);

  send_on_value_changed(230, movement);
  send_on_value_changed(240, di1);
  send_on_value_changed(241, di2);

  send_on_condition(560, { return ((bool)((uint32_t)temperature - (uint32_t)last_temp_value) > ((uint32_t)get<uint16_t>(temp_hyst))); }, NULL);
  send_on_condition(561, { return ((bool)((uint32_t)humidity - (uint32_t)last_humi_value) > ((uint32_t)get<uint16_t>(humi_hyst))); }, NULL);
  send_on_condition(562, { return ((bool)((uint32_t)luminosity - (uint32_t)last_lux_value) > ((uint32_t)get<uint16_t>(lux_hyst))); }, NULL);

  return true;
}

IRAM_ATTR
void step()
{
}

}
