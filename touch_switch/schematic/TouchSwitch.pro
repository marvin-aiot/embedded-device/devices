EAGLE AutoRouter Statistics:

Job           : /opt/Marvin/Hardware/STM32/Schematics/TouchSwitch.brd

Start at      : 11:34:27 (4/22/18)
End at        : 11:36:54 (4/22/18)
Elapsed time  : 00:02:23

Signals       :    65   RoutingGrid: 3.47638 mil  Layers: 2
Connections   :   121   predefined:  32 ( 0 Vias )

Router memory :   3009784

Passname          : TopRouter     Route Optimize1 Optimize2 Optimize3 Optimize4 Optimize5 Optimize6 Optimize7 Optimize8 Optimize9Optimize10Optimize11Optimize12

Time per pass     :  00:00:18  00:00:06  00:00:09  00:00:09  00:00:10  00:00:09  00:00:10  00:00:10  00:00:10  00:00:10  00:00:10  00:00:10  00:00:12  00:00:10
Number of Ripups  :         0         0         0         0         0         0         0         0         0         0         0         0         0         0
max. Level        :         0         0         0         0         0         0         0         0         0         0         0         0         0         0
max. Total        :         0         0         0         0         0         0         0         0         0         0         0         0         0         0

Routed            :        42        85        89        89        89        89        89        89        89        89        89        89        89        89
Vias              :         0        33        42        33        32        32        32        32        32        32        32        32        32        32
Resolution        :    61.2 %    96.7 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %   100.0 %

Final             : 89.7% finished. Polygons may have fallen apart.
