
#include <MPU6050.h>
#include <marvin_device.h>

using namespace MPU6050;

namespace marvin
{

// Uncomment to collect CSV data over Serial
#define MPU_COLLECT_SERIAL

extern Marvin* marvin;
static MPU6050::MPU6050 mpu = MPU6050::MPU6050();

struct mpu_data_derivate
{
  double x_accel;
  double y_accel;
  double z_accel;
  double x_gyro;
  double y_gyro;
  double z_gyro;
  double temp;
};

typedef std::pair<unsigned long, mpu_data> sensor_data;

//static bool background_initialized = false;

enum classification
{
  background,
  onset,
  peak
};

/*
 * https://github.com/godstale/retroband/blob/master/Arduino/RetroBand_Arduino/RetroBand_Arduino.ino
*/

enum params
{
  bed_movement_send_interval,
  data_read_interval,
  occupied
};

static const uint8_t baseline_inertia                          = 63;
static const unsigned long  def_bed_movement_send_interval     = 30;
static const unsigned long  def_data_read_interval             = 25;

static unsigned long  last_bed_movement_sent                   = 0;
static sensor_data sensor_baseline;

void send_bed_occupied_msg()
{
  DeviceMessage581 message = DeviceMessage581(marvin->get_device_id(), marvin->get<bool>(occupied));

  //marvin->send(&message);
  last_bed_movement_sent = marvin->now();
}

#ifdef never
mpu_data_derivate calculate_derivate(const mpu_data prev, const mpu_data cur, unsigned long curr_timestamp)
{
  uint32_t time_dif = curr_timestamp - previous_data_timestamp;

  mpu_data_derivate deriv;
  deriv.x_accel = static_cast<double>(cur.x_accel - prev.x_accel) / time_dif;
  deriv.y_accel = static_cast<double>(cur.y_accel - prev.y_accel) / time_dif;
  deriv.z_accel = static_cast<double>(cur.z_accel - prev.z_accel) / time_dif;
  deriv.x_gyro = static_cast<double>(cur.x_gyro - prev.x_gyro) / time_dif;
  deriv.y_gyro = static_cast<double>(cur.y_gyro - prev.y_gyro) / time_dif;
  deriv.z_gyro = static_cast<double>(cur.z_gyro - prev.z_gyro) / time_dif;
  deriv.temp = static_cast<double>(cur.temp - prev.temp) / time_dif;

  return std::move(deriv);
}
#endif

void step()
{
  unsigned long now = millis();
  mpu_data data = mpu.read();

  #ifdef MPU_COLLECT_SERIAL
    Serial.printf( "%lu,%d,%d,%d,%d,%d,%d,%d\r\n",
      now, data.x_accel, data.y_accel, data.z_accel,
      data.x_gyro, data.y_gyro, data.z_gyro, data.temp);
  #endif

#ifdef never
  int16_t x_accel = data.x_accel;
  int16_t y_accel = data.y_accel;
  int16_t z_accel = data.z_accel;
  int16_t x_gyro = data.x_gyro;
  int16_t y_gyro = data.y_gyro;
  int16_t z_gyro = data.z_gyro;
  int16_t temp = data.temp;

  data.x_accel -= sensor_baseline.x_accel;
  data.y_accel -= sensor_baseline.y_accel;
  data.z_accel -= sensor_baseline.z_accel;
  data.x_gyro -= sensor_baseline.x_gyro;
  data.y_gyro -= sensor_baseline.y_gyro;
  data.z_gyro -= sensor_baseline.z_gyro;
  data.temp -= sensor_baseline.temp;

  sensor_baseline.x_accel = ((sensor_baseline.x_accel * baseline_inertia) +
                              x_accel) /
                             (baseline_inertia + 1);
  sensor_baseline.y_accel = ((sensor_baseline.y_accel * baseline_inertia) +
                              y_accel) /
                             (baseline_inertia + 1);
  sensor_baseline.z_accel = ((sensor_baseline.z_accel * baseline_inertia) +
                              z_accel) /
                             (baseline_inertia + 1);
  sensor_baseline.x_gyro = ((sensor_baseline.x_gyro * baseline_inertia) +
                             x_gyro) /
                            (baseline_inertia + 1);
  sensor_baseline.y_gyro = ((sensor_baseline.y_gyro * baseline_inertia) +
                             y_gyro) /
                            (baseline_inertia + 1);
  sensor_baseline.z_gyro = ((sensor_baseline.z_gyro * baseline_inertia) +
                             z_gyro) /
                            (baseline_inertia + 1);
  sensor_baseline.temp = ((sensor_baseline.temp * baseline_inertia) +
                             temp) /
                            (baseline_inertia + 1);

  mpu_data_derivate derivate = calculate_derivate(previous_data, data, now);
  double abs_sum = abs(derivate.x_accel) + abs(derivate.y_accel) + abs(derivate.z_accel) +
                   abs(derivate.x_gyro) + abs(derivate.y_gyro) + abs(derivate.z_gyro) +
                   abs(derivate.temp);

  double sum = derivate.x_accel + derivate.y_accel + derivate.z_accel +
               derivate.x_gyro + derivate.y_gyro + derivate.z_gyro +
               derivate.temp;

  double accel_sum = derivate.x_accel + derivate.y_accel + derivate.z_accel;

  double gyro_sum = derivate.x_gyro + derivate.y_gyro + derivate.z_gyro;

  //classification classif = ((abs_sum < 50) ? background :
  //                                           (abs_sum > 500) ? peak :
  //                                                             onset);

#ifdef MPU_COLLECT_SERIAL
   Serial.printf( "%lu,%.2f,%.2f,%.2f,%.2f\r\n",
     now, abs_sum, sum, accel_sum, gyro_sum);
#endif

  //sensor_sum += abs_sum;
  //classification_history[history_position] = classif;
  //++history_position;

  if (history_position >= 5)
  {
    history_position = 0;

    uint8_t count_background = 0;
    uint8_t count_onset = 0;
    uint8_t count_peak = 0;

    for (uint8_t n = 0; n < 5; ++n)
    {
      if (classification_history[n] == background)
      {
        ++count_background;
      }
      else if (classification_history[n] == onset)
      {
        ++count_onset;
      }
      else if (classification_history[n] == peak)
      {
        ++count_peak;
      }
    }

    classification res_class = background;
    if (count_peak >= 2)
    {
      res_class = peak;
    }
    else if (count_onset >= 2)
    {
      res_class = onset;
    }

#ifdef MPU_COLLECT_SERIAL
    Serial.printf( "S,%lu,%.2f,%d\r\n",
      now, sensor_sum, res_class);
#endif

    sensor_sum = 0;
  }

  /*
  if (last_classification != classif)
  {
    last_classification_change = now;
    last_classification = classif;
  }
  */

  previous_data_timestamp = now;
  previous_data = data;

  //

#endif

/*
    // 1. Apply a low pass filter

    // 2. Remove baseline wander and gravity components
    if (!background_initialized)
    {
      background = data;
    }
    else
    {
      //data -= background;
      //background += data / 50;
    }

    // 3. Calculate the sum of the three axes for acceleration and gyro
    condensed_mpu_data cmd = condensed_mpu_data();
    cmd.accel = data.x_accel;
    cmd.accel += data.y_accel;
    cmd.accel += data.z_accel;
    cmd.gyro = data.x_gyro;
    cmd.gyro += data.y_gyro;
    cmd.gyro += data.z_gyro;
    cmd.temp = data.temp;

    // 4. Integrate the values over 2 and 4 seconds

    // 5. Extract features over a window of 30 seconds

    // 6. Decide whether someone is laying in the bed

    // @TODO Here we actually need a way to identify peope
    //       How can we separate people sitting or laying in the bed
    //       from objects placed on it?
    //       What about putting a baby in the bed?

    if ((cmd.accel.pos_max > 1000 ||
         cmd.accel.neg_max < -1000 ||
         cmd.gyro.pos_max > 1000 ||
         cmd.gyro.neg_max < -1000) &&
         !marvin->get<bool>(occupied))
    {
      marvin->set<bool>(occupied, true);
    }
    else if (marvin->get<bool>(occupied))
    {
      marvin->set<bool>(occupied, false);
    }
*/
}

bool initialize()
{
  // Device initialization
  mpu.setup();

  marvin->register_parameter<unsigned long>  ("bed_movement_send_interval",        bed_movement_send_interval,           "", def_bed_movement_send_interval);
  marvin->register_parameter<unsigned long>  ("data_read_interval",                data_read_interval,                   "", def_data_read_interval);
  marvin->register_variable<bool>            ("occupied",                          occupied,                             "", false);

  sensor_baseline = std::make_pair<unsigned long, mpu_data>
                       (millis(), std::move(mpu.read()));

  return true;
}

bool shutdown()
{
  return true;
}

}
