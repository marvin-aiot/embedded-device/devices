/*
 * Settings
 * 
 * Generic ESP8266
 * 80MHz
 * 1M (64K SPIFFS)
 * Flash Mode DIO
 * 
 */

#define CAN_BUS

#include <marvin_device.h>
#include <DHT.h>

namespace marvin
{

static const unsigned int movement_trigger_pin = 5;
static const unsigned int dht_pin = 2;  // Device 13 requires Pin 14, all others Pin 2!

extern Marvin* marvin;

enum params
{
  temp_send_interval,
  temp_hyst,
  humi_send_interval,
  humi_hyst,
  lux_send_interval,
  lux_hyst,
  movement_send_interval,
  movement
};

static DHT dht(dht_pin, DHT22);

static unsigned int   last_dht_read      = 0;
static unsigned short temperature        = 0;
static unsigned int   last_temp_sent     = 0;
static unsigned short last_temp_value    = 0;
static unsigned short humidity           = 0;
static unsigned int   last_humi_sent     = 0;
static unsigned short last_humi_value    = 0;
static unsigned short luminosity         = 0;
static unsigned int   last_lux_sent      = 0;
static unsigned int   last_lux_read      = 0;
static unsigned short last_lux_value     = 0;
static unsigned int   last_movement_sent = 0;

void update_dht_sensor()
{
  if (marvin->now() - last_dht_read < 3)
  {
    return;
  }

  float h = dht.readHumidity();
  float t = dht.readTemperature();

  last_dht_read = marvin->now();



  if (isnan(h))
  {
    LOG(F("Error reading DHT humidity data."));
    //humidity = 0;
  }
  else
  {
    humidity = floor(655 * h);
  }

  if (isnan(t))
  {
    LOG(F("Error reading DHT temperature data."));
    //temperature = 0;
  }
  else
  {
    temperature = floor((100 * t) + 27300);
  }
}

void send_temperature_msg()
{
  DeviceMessage560 message = DeviceMessage560(marvin->get_device_id(), temperature);

  marvin->send(&message);
  last_temp_sent = marvin->now();
  last_temp_value = temperature;
}

void send_humidity_msg()
{
  DeviceMessage561 message = DeviceMessage561(marvin->get_device_id(), humidity);

  marvin->send(&message);
  last_humi_sent = marvin->now();
  last_humi_value = humidity;
}

void send_luminosity_msg()
{
  DeviceMessage562 message = DeviceMessage562(marvin->get_device_id(), luminosity);

  marvin->send(&message);
  last_lux_sent = marvin->now();
  last_lux_value = luminosity;
}

void send_movement_msg()
{
  DeviceMessage230 message = DeviceMessage230(marvin->get_device_id(), marvin->get<bool>(movement, true));

  marvin->send(&message);
  last_movement_sent = marvin->now();
}

void step()
{
  // Check which of the monitored parameters have changed
  //

  unsigned int now = marvin->now();

  // MOVEMENT
  if (now - last_movement_sent >  marvin->get<unsigned int>(movement_send_interval) ||
      marvin->is_modified(movement))
  {
    send_movement_msg();
    delay(1);
  }

  update_dht_sensor();

  // TEMPERATURE
  if ((now - last_temp_sent >  marvin->get<unsigned int>(temp_send_interval)) ||
      (temperature - last_temp_value > marvin->get<unsigned short>(temp_hyst)))
  {
    send_temperature_msg();
    delay(1);
  }

  // HUMIDITY
  if ((now - last_humi_sent > marvin->get<unsigned int>(humi_send_interval)) ||
      (humidity - last_humi_value > marvin->get<unsigned short>(humi_hyst)))
  {
    send_humidity_msg();
    delay(1);
  }

  // LUMINOSITY
  if (now - last_lux_read > 1)
  {
    //luminosity = (unsigned short) analogRead(A0);
    last_lux_read = now;
  }

  if ((now - last_lux_sent > marvin->get<unsigned int>(lux_send_interval)) ||
      (luminosity < last_lux_value - marvin->get<unsigned short>(lux_hyst)) ||
      (luminosity > last_lux_value + marvin->get<unsigned short>(lux_hyst)))
  {
    send_luminosity_msg();
    delay(1);
  }
}

void movement_interrupt()
{
  marvin->set<bool>(movement, digitalRead(movement_trigger_pin) != 0);
}

bool initialize()
{
  // Device initialization
  dht.begin();
  pinMode(movement_trigger_pin, INPUT);
  attachInterrupt(digitalPinToInterrupt(movement_trigger_pin), movement_interrupt, CHANGE);

  marvin->register_parameter<unsigned int>  ("temp_send_interval",     temp_send_interval,          "",       30);
  marvin->register_parameter<unsigned short>("temp_hyst",              temp_hyst,                   "",       100);
  marvin->register_parameter<unsigned int>  ("humi_send_interval",     humi_send_interval,          "",       30);
  marvin->register_parameter<unsigned short>("humi_hyst",              humi_hyst,                   "",       100);
  marvin->register_parameter<unsigned int>  ("lux_send_interval",      lux_send_interval,           "",       30);
  marvin->register_parameter<unsigned short>("lux_hyst",               lux_hyst,                    "",       100);
  marvin->register_parameter<unsigned int>  ("movement_send_interval", movement_send_interval,      "",       30);
  marvin->register_variable <bool>          ("movement",               movement,                    "",       false);

  return true;
}

bool shutdown()
{
  return true;
}

}
