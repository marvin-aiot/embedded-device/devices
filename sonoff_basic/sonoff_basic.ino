/*
 * Settings
 *
 * Generic ESP8265
 * 80MHz
 * 1M (64K SPIFFS)
 * Flash Mode DIO
 *
 *
 * @REVIEW
 *    The logic behind the 'contact' parameter is flawed. It must not be a boolean, but an enum 
 *    to include permanent setting of the relais (like the 'manual_on' relais state of the SONOFF Switch).
 *    
 */
 
#include <marvin.h>

namespace marvin
{

const char* json_temperature    PROGMEM = "TEMPERATURE";
const char* json_contact        PROGMEM = "CONTACT";
const char* json_luminosity     PROGMEM = "LUMINOSITY";
const char* json_movement       PROGMEM = "MOVEMENT";
const char* json_state          PROGMEM = "STATE";

enum params
{
  state_send_interval,
  ilumination_source,
  movement_source,
  contact_source,
  activity,
  luminosity,
  luminosity_threshold,
  relais_timeout,
  auto_mode_disabled,
  relais_state
};

enum relais_state_t
{
  off,
  manual_on,
  auto_on
};

static const uint8_t  relais_pin              = GPIO3; // Pin 12

define_message_generator(547,
{
  Message msg = make_message(547);
  msg->set_payload(json_state, get<relais_state_t>(relais_state));
  success = send(msg);
});

IRAM_ATTR
void handle_lux_change()
{
  // Check whether we schould turn the relais on.
  if ((get<uint16_t>(luminosity, true) <
       get<uint16_t>(luminosity_threshold, true)) &&
      get<relais_state_t>(relais_state) == off &&
      get<bool>(auto_mode_disabled) == false &&
      get<bool>(activity) == true)
  {
    set<relais_state_t>(relais_state, auto_on);
  }
}

create_delayed_timer(auto_on_timeout,
  {
    set<relais_state_t>(relais_state, off);
    disable_task(timer_id_from_timer_name(auto_on_timeout));
  });
  
create_onchange_task(activity,
{
    reset_modified(activity);

    if (get<bool>(activity) == false)
    {
      return;
    }

    set<bool>(activity, false);

    if (get<bool>(auto_mode_disabled))
    {
      return;
    }

    relais_state_t state = get<relais_state_t>(relais_state);

    if (state == auto_on)
    {
      reschedule_task_by_id(timer_id_from_timer_name(auto_on_timeout), 
                            NOW_MILLIS + ((uint32_t)get<uint32>(relais_timeout)));
    }
    else if (state == off &&
              (get<uint16_t>(luminosity) <
               get<uint16_t>(luminosity_threshold)))
    {
      // @REVIEW This will not work as expected with a contact input, 
      //         as it must not depend on the luminosity
      set<relais_state_t>(relais_state, auto_on);
    }
  });

create_onchange_task(luminosity,
  {
      reset_modified(luminosity);
      handle_lux_change();
  });

create_onchange_task(luminosity_threshold,
  {
    reset_modified(luminosity_threshold);
    handle_lux_change();
  });

create_onchange_task(relais_state,
  {
    relais_state_t state = get<relais_state_t>(relais_state, true);
    FLOG_TRACE(F("Relais state changed to %s\r\n"), state == off ? "off" : state == auto_on ? "auto" : "manual");

    disable_task(timer_id_from_timer_name(auto_on_timeout));

    digitalWrite(relais_pin, (state == off) ? LOW : HIGH);
    send(547);

    if (state == auto_on)
    {
      reschedule_task_by_id(timer_id_from_timer_name(auto_on_timeout), 
                            NOW_MILLIS + ((uint32_t)get<uint32>(relais_timeout)));
      enable_task(timer_id_from_timer_name(auto_on_timeout));
    }
  });

IRAM_ATTR
uint16_t parse_ilumination_message(const Message msg)
{
  // @TODO There might be a logic to handle luminosity from multiple sources: min, max, avg etc.
  return msg->get_payload<uint16_t>(json_luminosity);
}

IRAM_ATTR
bool parse_movement_message(const Message msg)
{
  return msg->get_payload<bool>(json_movement);
}

IRAM_ATTR
bool parse_contact_message(const Message msg)
{
  return msg->get_payload<bool>(json_contact);
}

void on_run_level_changed(run_level_t run_level)
{
    if (run_level == RL0 ||
        run_level == RL1)
    {
      set<bool>(activity, false);
      set<relais_state_t>(relais_state, off);
      digitalWrite(relais_pin, LOW);
      
      disable_message_generator(547);
      
      // Disable all timers
      disable_delayed_timer(auto_on_timeout);
          
      disable_onchange_task(activity);
      disable_onchange_task(luminosity);
      disable_onchange_task(luminosity_threshold);
      disable_onchange_task(relais_state);
    }

    if (run_level == RL2)
    {
      // Enable all interrupts and timers
      enable_message_generator(547);

      enable_delayed_timer(auto_on_timeout);
      
      enable_onchange_task(activity);
      enable_onchange_task(luminosity);
      enable_onchange_task(luminosity_threshold);
      enable_onchange_task(relais_state);
      
      handle_lux_change();

      send(547);
      
    }
}

bool initialize()
{
  // Device initialization
  pinMode(relais_pin, OUTPUT);

  digitalWrite(relais_pin, LOW);

  register_parameter<MessageSource>       ("ilumination_source",    ilumination_source,       "", MessageSource(0, 0));
  register_parameter<MessageSource>       ("movement_source",       movement_source,          "", MessageSource(0, 0));
  register_parameter<MessageSource>       ("contact_source",        contact_source,           "", MessageSource(0, 0));
  register_parameter<uint16_t>            ("luminosity_threshold",  luminosity_threshold,     "", 420);
  register_parameter<uint32_t>            ("state_send_interval",   state_send_interval,      "", 300000);
  register_parameter<uint32_t>            ("relais_timeout",        relais_timeout,           "", 60000);

  register_variable<bool>                 ("activity",              activity,                 "", false);
  register_variable<uint16_t>             ("luminosity",            luminosity,               "", 0);
  register_variable<bool>                 ("auto_mode_disabled",    auto_mode_disabled,       "", false);
  register_variable<relais_state_t>       ("relais_state",          relais_state,             "", off);

  register_message_source<uint16_t>       (ilumination_source,      luminosity,   (uint16_t (*)(const Message)) parse_ilumination_message);
  register_message_source<bool>           (movement_source,         activity,     (bool (*)(const Message)) parse_movement_message);
  register_message_source<bool>           (contact_source,          activity,     (bool (*)(const Message)) parse_contact_message);

  register_message_generator(547, get<uint16_t>(state_send_interval));
  
  initialize_delayed_timer(auto_on_timeout, get<uint32>(relais_timeout));

  initialize_onchange_task(activity);
  initialize_onchange_task(luminosity);
  initialize_onchange_task(luminosity_threshold);
  initialize_onchange_task(relais_state);

  return true;
}

IRAM_ATTR
void step()
{
}

}
