
#include <marvin_device.h>

namespace marvin
{

static const unsigned int movement_trigger_pin = 5;
static const unsigned int dht_pin = 2;

extern Marvin* marvin;

enum params
{
  air_pressure,
  air_pressure_send_interval,
  air_pressure_threshold
};

static unsigned int   last_air_pressure_sent      = 0;
static unsigned short last_air_pressure_value     = 0;

void send_air_pressure_msg()
{
  DeviceMessage563 message = DeviceMessage563(marvin->get_device_id(), marvin->get<unsigned short>(air_pressure, true));

  marvin->send(&message);
  last_air_pressure_value = marvin->get<unsigned short>(air_pressure);
  last_air_pressure_sent = marvin->now();
}

void step()
{
  unsigned int now = marvin->now();

  // AIR PRESSURE
  if ((now - last_air_pressure_sent >  marvin->get<unsigned int>(air_pressure_send_interval)) ||
      (last_air_pressure_value - marvin->get<unsigned short>(air_pressure) > marvin->get<unsigned short>(air_pressure_threshold)))
  {
    send_air_pressure_msg();
    delay(1);
  }

  // AIR QUALITY

  // AIR DUST
}

bool initialize()
{
  // Device initialization

  marvin->register_parameter<unsigned short>("air_pressure_threshold",     air_pressure_threshold,      "",       100);
  marvin->register_parameter<unsigned short>("air_pressure_send_interval", air_pressure_send_interval,  "",       30);
  marvin->register_parameter<unsigned short>("air_pressure",               air_pressure,                "",       100);

  return true;
}

bool shutdown()
{
  return true;
}

}
