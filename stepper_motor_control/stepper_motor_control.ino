/*
 * Settings
 *
 * Generic ESP8266
 * 80MHz
 * 1M (64K SPIFFS)
 * Flash Mode DIO
 *
 */
#include <marvin.h>

namespace marvin
{

enum params
{
};

bool initialize()
{
  return true;
}

void step()
{
}

bool shutdown()
{
  return true;
}

}
