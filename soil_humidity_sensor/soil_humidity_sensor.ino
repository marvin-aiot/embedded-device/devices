/*
 * Settings
 *
 * Generic ESP8266
 * 80MHz
 * 1M (64K SPIFFS)
 * Flash Mode DIO
 *
 */
#include <marvin.h>

#define HAS_TANK_N_PUMP true

#ifndef HAS_TANK_N_PUMP
  #define HAS_TANK_N_PUMP false
#endif

namespace marvin
{

const char* json_humidity       PROGMEM = "HUMIDITY";

#if HAS_TANK_N_PUMP
const char* json_contact        PROGMEM = "CONTACT";
const char* json_state          PROGMEM = "STATE";
#endif

enum params
{
  humi_send_interval,
  humi_hyst,
  humi_low,
  humi_high,

#if HAS_TANK_N_PUMP
  pump_send_interval,
  water_level_send_interval,
  low_water,
#endif
};

static const uint8_t status_led_pin = GPIO7;  // Check on actual board!
static const uint8_t humidity_sensor_pin = AIN;

#if HAS_TANK_N_PUMP
static const uint8_t water_level_low_sensor_pin = GPIO1;  // Check on actual board!
static const uint8_t water_pump_pin = GPIO3;  // Check on actual board!

static bool pump_status = false;
#endif

static uint16_t humidity           = 0;
static uint16_t last_humi_value    = 0;

#if HAS_TANK_N_PUMP
define_message_generator(547,
{
  Message msg = make_message(547);
  msg->set_payload(json_state, pump_status);
  success = send(msg);
});

define_message_generator(580,
{
  Message msg = make_message(580);
  msg->set_payload(json_contact, low_water);
  success = send(msg);
});
#endif

define_message_generator(561,
{
  Message msg = make_message(561);
  msg->set_payload(json_humidity, humidity);

  success = send(msg);
  last_humi_value = humidity;
});

#if HAS_TANK_N_PUMP
IRAM_ATTR
void low_water_interrupt()
{
  bool state = (digitalRead(water_level_low_sensor_pin) == HIGH);

  if (state == get<bool>(low_water))
  {
    return;
  }

  set<bool>(low_water, state);
}
#endif

void on_run_level_changed(run_level_t run_level)
{
    if (run_level == RL2)
    {
      
    }
    else
    {
      
    }
}

bool initialize()
{
  // Device initialization
  pinMode(status_led_pin, OUTPUT);

#if HAS_TANK_N_PUMP
  pinMode(water_pump_pin, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(water_level_low_sensor_pin), low_water_interrupt, CHANGE);
#endif

  register_parameter<uint32_t>("humi_send_interval",         humi_send_interval,          "",       1800000);
  register_parameter<uint16_t>("humi_hyst",                  humi_hyst,                   "",       100);
  register_parameter<uint16_t>("humi_low",                   humi_low,                    "",       100);
  register_parameter<uint16_t>("humi_high",                  humi_high,                   "",       1000);

  register_message_generator(561, get<uint32_t>(humi_send_interval));

#if HAS_TANK_N_PUMP
  register_parameter<uint32_t>("pump_send_interval",         pump_send_interval,          "",       3600000);
  register_parameter<uint32_t>("water_level_send_interval",  water_level_send_interval,   "",       3600000);
  register_parameter<bool>    ("low_water",                  low_water,                   "",       false);

  register_message_generator(547, get<uint32_t>(pump_send_interval));
  register_message_generator(580, get<uint32_t>(water_level_send_interval));
#endif

  send_on_condition(561, { return ((uint32_t)humidity - (uint32_t)last_humi_value) > ((uint32_t)get<uint16_t>(humi_hyst)); }, NULL); // @REVIEW This will send non-stop!!

  define_delayed_task([](task_id_t, void*) -> void
  {
    humidity = (uint16_t) analogRead(humidity_sensor_pin);
  }, 60000, true, NULL);

  // Use LED to show humidity based on LOW/HIGH thresholds
  // Humidity below humi_low makes LED blink
  // Humidity starts to rise as user pour water
  // Humidity reaches ((humi_high - humi_low) /2), making LED turn off
  // Humidity rises above humi_high, turning LED back on
  // Humidity stops rising (even if above humi_high), turning LED off

  // Add option to control a water pump!


#if HAS_TANK_N_PUMP
  define_onchange_task([](task_id_t, void*) -> void
    {
      reset_modified(low_water);

      if (get<bool>(low_water))
      {
        digitalWrite(water_pump_pin, LOW);
        digitalWrite(status_led_pin, HIGH);
      }
      else
      {
        digitalWrite(status_led_pin, LOW);
      }
    }, low_water);
#endif

  return true;
}

void step()
{
}


}
