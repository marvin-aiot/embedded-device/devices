
/*
 * 
IROM   : 316208          - code in flash         (default or ICACHE_FLASH_ATTR) 
IRAM   : 30344   / 32768 - code in IRAM          (ICACHE_RAM_ATTR, ISRs...) 
DATA   : 1356  )         - initialized variables (global, static) in RAM/HEAP 
RODATA : 3736  ) / 81920 - constants             (global, static) in RAM/HEAP 
BSS    : 27056 )         - zeroed variables      (global, static) in RAM/HEAP 



 * TODOs
 * How to add a fade-in/fade-out of the LEDs?
 * How to detect someone else using the stairs after the first (initial trigger) has left the stairs?
 * 
 */
 
#include <NeoPixelBrightnessBus.h>
#include <NeoPixelSegmentBus.h>
#include <NeoPixelAnimator.h>
#include <NeoPixelBus.h>

#include <marvin.h>

namespace marvin
{

const char* json_state          PROGMEM = "STATE";

enum params
{
  number_of_leds,
  led_cascade_time,
  led_keep_on_time,
  state_send_interval,
  movement_send_interval,
  top_movement,
  bottom_movement,
  luminosity
};

enum phase_t
{
  turn_on,
  turn_off,
  rest
};

static const uint8_t led_pin               = GPIO3; // Pin 12
static const uint8_t top_detector_pin      = GPIO4;
static const uint8_t bottom_detector_pin   = GPIO5;

static const int8_t down = 1;
static const int8_t up = -1;
static int8_t movement_direction = up;
static phase_t current_phase = rest;

uint8_t current_led_index = 0;

NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart1Ws2812xMethod> * pixel_bus = nullptr;
RgbColor * lux = nullptr;
RgbColor dark = RgbColor(0);

task_id_t led_cascade_timer = 0;

// Send 230 & 231 for top and bottom movement
define_message_generator(230,
{
  Message msg = make_message(230);
  msg->set_payload(json_state, top_movement);
  success = send(msg);
});

define_message_generator(231,
{
  Message msg = make_message(231);
  msg->set_payload(json_state, bottom_movement);
  success = send(msg);
});

define_message_generator(547,
{
  Message msg = make_message(547);
  msg->set_payload(json_state, current_led_index != 0);
  success = send(msg);
});

task_id_t start_led_cascade_timer_disabled()
{
  led_cascade_timer = define_delayed_task([](task_id_t, void*) -> void 
    {
      // Turn on LEDs current_led_index
      pixel_bus->SetPixelColor(current_led_index, (current_phase == turn_on ? *lux : dark));
      pixel_bus->Show();

      if (((movement_direction == down && current_led_index == 0) || 
           (movement_direction == up && current_led_index >= get<uint8_t>(number_of_leds) - 1)))
      {
        if (current_phase == turn_on)
        {
          reschedule_task_by_id(led_cascade_timer, NOW_MILLIS + get<uint32_t>(led_keep_on_time));
          current_phase = turn_off;
        }
        else // if (current_phase == turn_off)
        {
          disable_task(led_cascade_timer);
          current_phase = rest;
        }
      }
      else
      {
        current_led_index += current_phase;
      }
      
    }, get<uint16_t>(led_cascade_time), true, nullptr, false);
  return led_cascade_timer;
}

IRAM_ATTR
void top_movement_interrupt()
{
  bool state = (digitalRead(top_detector_pin) == LOW);

  if (state == get<bool>(top_movement))
  {
    return;
  }

  set<bool>(top_movement, state);
}

IRAM_ATTR
void bottom_movement_interrupt()
{
  bool state = (digitalRead(bottom_detector_pin) == LOW);

  if (state == get<bool>(bottom_movement))
  {
    return;
  }

  set<bool>(bottom_movement, state);
}

void on_run_level_changed(run_level_t run_level)
{  
  if (run_level == RL0)
  {
    detachInterrupt(digitalPinToInterrupt(top_detector_pin));
    detachInterrupt(digitalPinToInterrupt(bottom_detector_pin));
    
    // Set all LEDs to dark
    uint8_t max_i = get<uint8_t>(number_of_leds) - 1;
    for (uint8_t i = 0; i <= max_i; ++i)
    {
      pixel_bus->SetPixelColor(i, dark);
    }
  
    pixel_bus->Show();

    disable_message_generator(230);
    disable_message_generator(231);
    disable_message_generator(547);
    
  }
  else
  {
    attachInterrupt(digitalPinToInterrupt(top_detector_pin), top_movement_interrupt, CHANGE);
    attachInterrupt(digitalPinToInterrupt(bottom_detector_pin), bottom_movement_interrupt, CHANGE);

    
    enable_message_generator(230);
    enable_message_generator(231);
    enable_message_generator(547);
  }
}

bool initialize()
{
  pinMode(led_pin, OUTPUT);
  pinMode(top_detector_pin, INPUT);
  pinMode(bottom_detector_pin, INPUT);

  digitalWrite(led_pin, LOW);
  
  register_parameter<uint8_t>            ("number_of_leds",         number_of_leds,         "", 0);
  register_parameter<uint16_t>           ("led_cascade_time",       led_cascade_time,       "", 330);
  register_parameter<uint32_t>           ("led_keep_on_time",       led_keep_on_time,       "", 15000);
  register_parameter<uint32_t>           ("state_send_interval",    state_send_interval,    "", 30000);
  register_parameter<uint32_t>           ("movement_send_interval", movement_send_interval, "", 30000);
  register_parameter<uint16_t>           ("led_cascade_time",       led_cascade_time,       "", 330);
  register_parameter<uint8_t>            ("luminosity",             luminosity,             "", 100);
  
  register_variable<bool>                ("top_movement",           top_movement,           "", 0);
  register_variable<bool>                ("bottom_movement",        bottom_movement,        "", 0);

  lux = new RgbColor(get<uint8_t>(luminosity));
  pixel_bus = new NeoPixelBus<NeoGrbFeature, NeoEsp8266Uart1Ws2812xMethod>(get<uint8_t>(number_of_leds));
  
  register_message_generator(230, get<uint32_t>(movement_send_interval));
  register_message_generator(231, get<uint32_t>(movement_send_interval));
  register_message_generator(547, get<uint32_t>(state_send_interval));

  start_led_cascade_timer_disabled();

  define_onchange_task([](task_id_t, void*) -> void 
    {
      if(current_phase == rest)
      {
        movement_direction = down;
        current_phase = turn_on;
        current_led_index = 0;
        enable_task(led_cascade_timer);
      }
    }, top_movement);

  define_onchange_task([](task_id_t, void*) -> void 
    {
      if(current_phase == rest)
      {
        movement_direction = up;
        current_phase = turn_on;
        current_led_index = get<uint8_t>(number_of_leds) - 1;
        enable_task(led_cascade_timer);
      }
    }, bottom_movement);

  
  define_onchange_task([](task_id_t, void*) -> void 
    {
      // Reset?
      
    }, led_cascade_time);

  define_onchange_task([](task_id_t, void*) -> void 
    {
      // Reset?
      
    }, luminosity);

  return true;
}

IRAM_ATTR
void step()
{
}


}
