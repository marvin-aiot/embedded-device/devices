


| Name | ID | Data type | Default value |
|------|----|-----------|---------------|
| number_of_leds | 0000 | | 0 |
| state_send_interval | 0001 | | 30000 |
| movement_send_interval | 0002 | uint16 | 30000 |
| led_cascade_time | 0003 | uint16 | 330 |
| top_movement | 0004 | bool | 0 |
| bottom_movement | 0005  | bool | 0 |
| luminosity | 0006 | uint16 | 100 |
| restart_counter | 0100 | uint8 | 1 |
| hb_send_interval uint32 | 0101 | | 30000 |
| ap_ssid | 1000 | string | |
| ap_passwd | 1001 | string | |
| device_id | 9000 | uint8 | 0 |
| device_status | 9001 | | 0 |
